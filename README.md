# SETUP SMTP under LINUX

# 1. Search php.ini  
```
locate php.ini
```

# 2. You will find many php.ini, apply to all the php.ini that matters to you (in case you don't find the following rows)
```
SMTP = localhost
smtp_port = 25
```

# 3. install ssmtp
```
sudo apt install ssmtp
```

# 4. Replace the content of the file /etc/ssmtp/ssmtp.conf with the following info (in this example it's used this smtp service https://account.sendinblue.com/advanced/api)
```
# every mail to : address@a.com
# root=address@a.com
root=sandro.lu13@gmail.com

# server smtp:port
mailhub=smtp-relay.sendinblue.com:587

#rewriteDomain=hostname=username@gmail.com

UseSTARTTLS=YES

AuthUser=sandro.lu13@gmail.com

AuthPass=nKTgcrkEbsm63I7x 

FromLineOverride=YES
```

# TEST (PHP 7)
```
user@debian:~$ php -a
Interactive mode enabled

php > mail('sandro.lu13@gmail.com', 'test', 'test');
php >
```
